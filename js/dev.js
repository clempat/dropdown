//Mixins
// Create Global "extend" method
var extend = function (obj, extObj) {
    "use strict";
    var i, a, length;

    if (arguments.length > 2) {
        for (a = 1, length = arguments.length; a < length; a++) {
            extend(obj, arguments[a]);
        }
    } else {
        for (i in extObj) {
            if (extObj.hasOwnProperty(i)) {
                obj[i] = extObj[i];

            }
        }
    }
    return obj;
};

/*global window */
/*global document */
var DropDown = (function (window, document) {
    "use strict";
    var defaultSettings,
        dropDown,
        toggleList,
        selectValue,
        onClick,
        onListToggle,
        listToggled,
        onItemSelect,
        itemSelected,
        closeAll;

    defaultSettings = {
        position: 'auto',
        transition: false
    };

    //@TODO Check ie for Event
    onListToggle = new Event('onListToggle');
    listToggled = new Event('listeToggled');
    onItemSelect = new Event('onItemSelect');
    itemSelected = new Event('itemSelected');

    //Constructor
    dropDown = function (element, options) {
        var children, i;

        this.el = (typeof element === "object") ? element : document.getElementById(element);
        this.settings = extend(defaultSettings, options);
        this.name = this.el.getAttribute('data-name');

        // Prepare children
        children = this.el.childNodes;

        for (i = children.length - 1; i >= 0; i--) {
            switch (children[i].tagName) {
                case 'DIV':
                    this.valueContainer = children[i];
                    break;
                case 'UL':
                    this.list = children[i];
                    break;
            }
        }

    };


    onClick = function (e) {
        var fn;

        switch (e.target.tagName) {
            case 'DIV':
                fn = 'toggleList';
                break;
            case 'LI':
                fn = 'selectValue';
                break;
            default:
                return;
        }

        this[fn](e);

    };

    closeAll = function () {
    };

    //ToggleList Action
    toggleList = function (e) {
        this.el.dispatchEvent(onListToggle);

        if (e === false) {
            if (!this.list.classList.contains('hidden')) {
                this.list.classList.add('hidden');
            }
        } else {
            this.list.classList.toggle('hidden');
        }

        this.el.dispatchEvent(listToggled);
    };

    //SelectValue
    selectValue = function (e) {
        var input;
        this.el.dispatchEvent(onItemSelect);
        // Save In the object
        this.valueContainer.innerText = e.target.innerText;
        this.value = e.target.getAttribute('data-value');

        // Add the input
        if (this.input === undefined) {
            this.input = document.createElement('input');
            this.input.setAttribute('type', 'hidden');
            this.input.setAttribute('name', this.name);
        }
        this.input.setAttribute('value', this.value);
        this.el.appendChild(this.input);

        // Close DropDown
        this.toggleList(e);
        this.el.dispatchEvent(itemSelected);
    };

    //Events
    document.addEventListener("click", onClick, false);
    document.addEventListener("touchstart", onClick, false);

    // Prototype
    dropDown.prototype.constructor = dropDown;
    dropDown.prototype.selectValue = selectValue;
    dropDown.prototype.toggleList = toggleList;
    dropDown.prototype.toggleList = closeAll;

    return dropDown;
}(window, document));

// Extension
/*dropDown = function (window, document, parent) {
 "use strict";

 //Constructor
 dropDown = function (element, options) {
 parent.constructor.apply(this, [element, options]);

 }

 }(window, document, DropDown);*/

var dropDownObjects = (function (window, document) {
    "use strict";
    var dropDowns = document.getElementsByClassName('dropdown'),
        i,
        dropDownObjects = [],
        name;

    for (i = dropDowns.length - 1; i >= 0; i--) {
        name = dropDowns[i].getAttribute('data-name');
        dropDownObjects[name] = new DropDown(dropDowns[i]);
    }

    dropDowns[0].addEventListener('onListToggle', function () {
        console.log('rtot');
    });

    return dropDownObjects;
}(window, document));
