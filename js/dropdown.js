//Mixins
// Create Global "extend" method
var extend = function (obj, extObj) {
    "use strict";
    var i, a, length;

    if (arguments.length > 2) {
        for (a = 1, length = arguments.length; a < length; a++) {
            extend(obj, arguments[a]);
        }
    } else {
        for (i in extObj) {
            if (extObj.hasOwnProperty(i)) {
                obj[i] = extObj[i];

            }
        }
    }
    return obj;
};

/*global window */
/*global document */
/*global Event */
var DropDown = (function (window, document) {
    "use strict";
    var dropDown,
        toggleList,
        selectValue,
        onClick,
        closeAll,
        closeOther,
        touchStart,
        touchEnd,
        getHeight,
        getDropDown,
        toggleHover,
        toucheMove;

    //Constructor
    dropDown = function (element) {
        var children,
            i,
            maxHeigh,
            maxHeightBottom,
            maxHeightTop,
            body = document.body,
            html = document.documentElement,
            li,
            j,
            length;

        this.el = (typeof element === "object") ? element : document.getElementById(element);
        this.name = this.el.getAttribute('data-name');

        // Prepare children
        maxHeigh = Math.max(body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight);
        maxHeightBottom = maxHeigh - this.el.getBoundingClientRect().bottom - 2;
        maxHeightTop    = this.el.getBoundingClientRect().top - 2;
        children = this.el.childNodes;

        for (i = children.length - 1; i >= 0; i--) {
            switch (children[i].tagName) {
            case 'DIV':
                this.valueContainer = children[i];
                break;
            case 'UL':
                this.list = children[i];
                // Maybe min in settings
                if (maxHeightBottom > 100) {
                    children[i].style.maxHeight = maxHeightBottom + 'px';
                } else {
                    children[i].style.maxHeight = maxHeightTop + 'px';
                    children[i].style.top = -Math.min(maxHeightTop, getHeight(children[i])) + "px";
                }

                break;
            }
        }
    };

    onClick = function (e) {
        var fn,
            el = e.target,
            dropDown = getDropDown(el),
            li,
            i;

        if (!dropDown) {
            DropDown.prototype.closeAll();
            return;
        }
        li = dropDown.list.childNodes;
        //Reset pos
        dropDown.pos = dropDown.list.getBoundingClientRect().top;
        for (i = li.length - 1; i >= 0; i--) {
            if (li[i].tagName === 'LI') {
                dropDown.liHeight = li[i].getBoundingClientRect().height;
                break;
            }
        }

        switch (e.target.tagName) {
        case 'DIV':
            DropDown.prototype.closeOther(dropDown.name);
            fn = 'toggleList';
            break;
        case 'LI':
            fn = 'selectValue';
            break;
        default:
            return;
        }

        dropDown[fn](e);

    };

    // Touch event
    touchStart = function (e) {
        var el = e.target,
            dropDown = getDropDown(el);

        if (!dropDown) {
            DropDown.prototype.closeAll();
            return;
        }

        e.preventDefault();
        dropDown.toggleList(e);
        document.addEventListener('touchmove', toucheMove, false);
    };

    touchEnd = function (e) {
        var el = e.target,
            dropDown = getDropDown(el),
            selected = null,
            y = e.changedTouches[0].pageY,
            li,
            i,
            between1,
            between2;

        if (!dropDown) {
            DropDown.prototype.closeAll();
            return;
        }
        e.preventDefault();
        li = dropDown.list.childNodes;

        for (i = li.length - 1; i >= 0; i--) {
            if (li[i].tagName === "LI") {
                between1 =  li[i].getBoundingClientRect().top >= y && y > li[i].getBoundingClientRect().bottom;
                between2 =  li[i].getBoundingClientRect().top <= y && y < li[i].getBoundingClientRect().bottom;
                if (between1 || between2) {
                    selected = li[i];
                    break;
                }
            }
        }

        if (selected) {
            dropDown.selectValue(selected);
        } else {
            dropDown.toggleList(e);
        }
        document.removeEventListener('touchmove', toucheMove, false);
    };
    toucheMove = function (e) {
        if (this.position === 'bottom') {

        } else if (this.position === 'top') {

        }
    };
    toggleHover = function (e) {
        e.target.classList.toggle('hover');
    };

    closeAll = function () {
        var i;

        for (i = DropDown.dropDownObjects.all.length - 1; i >= 0; i--) {
            if (DropDown.dropDownObjects.all.hasOwnProperty(i)) {
                DropDown.dropDownObjects.all[i].toggleList(false);
            }
        }
    };

    closeOther = function (exceptName) {
        var i;
        for (i = DropDown.dropDownObjects.all.length - 1; i >= 0; i--) {
            if (DropDown.dropDownObjects.all.hasOwnProperty(i) && DropDown.dropDownObjects.map[exceptName] !== i) {
                DropDown.dropDownObjects.all[i].toggleList(false);
            }
        }
    };

    getHeight = function (element) {
        var el_top = element.top,
            el_height;

        if (element.classList.contains('hidden')) {
            element.top = -10000;
            element.classList.remove('hidden');
            el_height = element.getBoundingClientRect().height;
            element.classList.add('hidden');
            element.top = el_top;
            return el_height;
        }
        return element.getBoundingClientRect().height;
    };

    getDropDown = function (element) {
        var i = 0,
            dropDown = false,
            name;
        // Check if click is on one drop down or not
        while (element.tagName !== "HTML" && i < 4 && !element.hasAttribute('data-name')) {
            element = element.parentNode;
            i++;
        }

        if (element.tagName !== "HTML" && i < 4) {
            name = element.getAttribute('data-name');
            if (DropDown.dropDownObjects.map.hasOwnProperty(name)) {
                dropDown = DropDown.dropDownObjects.all[DropDown.dropDownObjects.map[name]];
            }
        }

        return dropDown;
    };

    //ToggleList Action
    toggleList = function (e) {
        if (e === false) {
            if (!this.list.classList.contains('hidden')) {
                this.list.classList.add('hidden');
            }
        } else {
            this.list.classList.toggle('hidden');
        }

    };

    //SelectValue
    selectValue = function (e) {
        var input;
        if (e instanceof Event) {
            // Save In the object
            this.valueContainer.innerHTML = e.target.innerText;
            this.value = e.target.getAttribute('data-value');
        } else {
            this.valueContainer.innerHTML = e.innerText;
            this.value = e.getAttribute('data-value');
        }


        // Add the input
        if (this.input === undefined) {
            this.input = document.createElement('input');
            this.input.setAttribute('type', 'hidden');
            this.input.setAttribute('name', this.name);
        }
        this.input.setAttribute('value', this.value);
        this.el.appendChild(this.input);

        // Close DropDown
        this.toggleList(e);
    };

    document.addEventListener('click', onClick, false);
    document.addEventListener('touchstart', touchStart, false);
    document.addEventListener('touchend', touchEnd, false);

    // Prototype
    dropDown.prototype.constructor = dropDown;
    dropDown.prototype.selectValue = selectValue;
    dropDown.prototype.toggleList = toggleList;
    dropDown.prototype.closeAll = closeAll;
    dropDown.prototype.closeOther = closeOther;

    return dropDown;
}(window, document));

var dropDownObjects = (function (window, document) {
    "use strict";
    var dropDowns = document.getElementsByClassName('dropdown'),
        i,
        dropDownObjects = {
            map: [],
            all: []
        },
        name;

    for (i = dropDowns.length - 1; i >= 0; i--) {
        name = dropDowns[i].getAttribute('data-name');
        dropDownObjects.map[name] = dropDownObjects.all.length;
        dropDownObjects.all.push(new DropDown(dropDowns[i]));
    }
    DropDown.dropDownObjects = dropDownObjects;
    DropDown.prototype.closeAll();

    return dropDownObjects;
}(window, document));
